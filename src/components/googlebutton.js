import React from 'react';


const styles={
    container:{
        border: "2px solid black",
        width:"80px",
        borderRadius: "30px"
    }
}

const GoogleButton = () => {

    const HandleClick = () =>{
        console.log("clicked");
    }
    const logo=require("./google-logo.jpg")

    return (
        <div 
            id="Container"
            onClick={HandleClick}
            style={styles.container}
        >
            <img 
                src={logo} 
                alt="Google Button Logo"
                height="20"
                width="20"
            />
        </div>
    )
}


export default GoogleButton;