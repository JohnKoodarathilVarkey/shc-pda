import React,{useState} from 'react';
import GoogleButton from './../../../components/googlebutton'
import './loginscreen.css'

const LoginScreen = ()=>{
    const [username , setusername] = useState("");
    const [password , setpassword] = useState("");
    
    const handleInputChange = event =>{
        const value = event.target.value; 
        if(event.target.name === "username")
            setusername(value);
        else
            setpassword(value);
    }

    const handleButtonPress = event => {
        console.log(username,password)
        event.preventDefault()
        
    }

    return(<div id="container">
        <div id="innerContainer">
            <div id="leftDiv">
                <div id="titleDiv">
                    <span>Login</span>
                    <span>Don't Have an Account? Create your Account</span>
                </div>
                <div id="formDiv">
                    <form>
                        <input 
                            type="text" 
                            name="username"
                            value={username} 
                            onChange={handleInputChange}     
                        />
                        <input 
                            type="password" 
                            name="password"
                            value={password} 
                            onChange={handleInputChange}     
                        />
                        <button 
                            type="submit"
                            name="signin" 
                            onClick={handleButtonPress}
                        >
                            Sign In
                        </button>
                    </form>
                    <div>
                        <span>Or Login With</span>
                        <div>
                            <GoogleButton />
                        </div>
                    </div>
                </div>
            </div>
            <div id="rightDiv">
            </div>
        </div>
    </div>)
}

export default LoginScreen;