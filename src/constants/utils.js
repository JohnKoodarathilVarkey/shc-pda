import { AsyncStorage } from "react-native";
import { Colors } from "react-native-paper";
import * as Facebook from "expo-facebook";
import * as Google from "expo-google-app-auth";
import * as AppleAuthentication from "expo-apple-authentication";
import moment from "moment";
import { AuthSession } from "expo";

import Test from "../assets/images/medical/cardiogram.png";
import Procedure from "../assets/images/medical/electrocardiogram.png";
import Pills from "../assets/images/medical/pills.png";
import Allergy from "../assets/images/medical/first-aid-kit.png";
import Diagnosis from "../assets/images/medical/medical-history.png";
import Appointment from "../assets/images/medical/red-cross.png";
import HeadIcon from "../assets/images/medical/head.png";
import env from "../environment";
import { Alert } from "react-native";

export const shcBlue = "#54c5f1";
export const shcBlue70 = "rgba(84, 197, 241, 0.7)";
export const shcRed = "#f06154";
export const shcRed30 = "rgba(240, 97, 84, 0.3)";
export const facebookBlue = "#4267b2";
const {
  baseApiUrl,
  iosClientId,
  androidClientId,
  googleApiUrl,
  facebookAppId,
  facebookApiUrl,
  facebookApiFields,
} = env;

export const FormatTimestamp = (timestamp) => {
  const timeSincePost = moment(timestamp).fromNow();
  if (moment(timestamp).isSame(moment(), "day")) {
    return `Today, ${timeSincePost}`;
  }
  return timeSincePost;
};

export const IsAfterToday = (timestamp) => {
  if (moment(timestamp).isAfter(moment(), "minute")) {
    return true;
  }
  return false;
};

export const SplitByCapitals = (text) => {
  const splitArray = text.split(/(?=[A-Z])/);
  let spacedText = "";
  for (let i = 0; i < splitArray.length; i++) {
    spacedText += ` ${splitArray[i]}`;
  }
  return spacedText;
};

export const GetIcon = (type) => {
  switch (type) {
    case "Test":
      return Test;

    case "Procedure":
      return Procedure;

    case "Medication":
      return Pills;

    case "Allergy":
      return Allergy;

    case "Diagnosis":
      return Diagnosis;

    case "Appointment":
      return Appointment;

    case "HeadInjuryForm":
    case "NDI":
    case "Back":
    case "pre_cer_PROMIS":
    case "pre_cer_expectations":
    case "pre_cer_NDI":
    case "pre_non_cer_PROMIS":
    case "pre_non_cer_expectations":
    case "pre_non_cer_ODI":
    case "post_cer_PROMIS":
    case "post_cer_expectations":
    case "post_cer_NDI":
    case "post_non_cer_PROMIS":
    case "post_non_cer_expectations":
    case "post_non_cer_Back":
      return HeadIcon;

    default:
      return Appointment;
  }
};

export const GetTypeColor = (type) => {
  switch (type) {
    case "Test":
      return Colors.green600;

    case "Procedure":
      return Colors.teal600;

    case "Medication":
      return Colors.purple600;

    case "Allergy":
      return Colors.red600;

    case "Diagnosis":
      return Colors.amber600;

    case "Appointment":
      return Colors.indigo600;

    case "HeadInjuryForm":
    case "Survey":
      return Colors.blue200;

    default:
      return Colors.grey600;
  }
};

export const GetContactHeader = (userType) => {
  switch (userType) {
    case "Doctor":
      return "Patients";

    case "Patient":
      return "Doctors";

    default:
      return "Contacts";
  }
};

export const GetProviderHeader = (userProvider) => {
  switch (userProvider) {
    case "facebook":
      return "Facebook";
      break;
    case "google":
      return "Google";
      break;
    case "securehealth":
      return "Secure Health";
    default:
      return "UNKNOWN";
  }
};

export const IsPatient = (userType) => {
  if (userType === "Patient") {
    return true;
  }
  return false;
};

export const IsSecureHealthProvider = (userProvider) => {
  if (userProvider === "securehealth") {
    return true;
  }
  return false;
};

export const IsSecureHealthEmail = async (email) => {
  const user = await GetUserByEmail(email, "securehealth");
  if(user===null){ 
    return false;
  }
  if (IsSecureHealthProvider(user.provider)) {
    return true;
  }
  const providerText = GetProviderHeader(user.provider);
  return alert(
    `This email address is registered with a ${providerText} account. Please reset your password using the ${providerText} services.`
  )
};

export const IsDarkMode = async () => {
  try {
    const theme = await AsyncStorage.getItem("UserTheme");
    if (theme === null || theme === undefined || theme === "false") {
      return false;
    }
    return true;
  } catch (err) {
    console.error(err);
  }
};

export const IsImageChainEnabled = async () => {
  try {
    const enabled = await AsyncStorage.getItem("ImageChainEnabled");
    if (enabled === null || enabled === undefined || enabled === "false") {
      return false;
    }
    return true;
  } catch (err) {
    console.error(err);
  }
};

export const IsSportMedicineEnabled = async () => {
  try {
    const enabled = await AsyncStorage.getItem("SportMedicineEnabled");
    if (enabled === null || enabled === undefined || enabled === "false") {
      return false;
    }
    return true;
  } catch (err) {
    console.error(err);
  }
};

export const SetDarkMode = async (value) => {
  try {
    await AsyncStorage.setItem("UserTheme", value.toString());
    console.log("UTILS: " + (await AsyncStorage.getItem("UserTheme")));
  } catch (err) {
    console.error(err);
  }
};

export const SetImageChainMode = async (value) => {
  try {
    await AsyncStorage.setItem("ImageChainEnabled", value.toString());
  } catch (err) {
    console.error(err);
  }
};

export const SetSportsMedicineMode = async (value) => {
  try {
    await AsyncStorage.setItem("SportMedicineEnabled", value.toString());
  } catch (err) {
    console.error(err);
  }
};

export const GetTimelinePayload = async (recordId, token, navigation) => {
  try {
    const response = await fetch(`${baseApiUrl}/timeline/${recordId}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const timelineItems = await response.json();

    if (
      !timelineItems ||
      (timelineItems.statusCode &&
        (timelineItems.statusCode === 401 || timelineItems.statusCode === 500))
    ) {
      navigation.pop();
      return;
    }

    if (timelineItems) {
      timelineItems.sort((a, b) => {
        return moment(b.date) - moment(a.date);
      });
    }
    return timelineItems;
  } catch (err) {
    console.error(err);
  }
};

export const GetMedicationPayload = async (recordId, token, navigation) => {
  try {
    const response = await fetch(
      `${baseApiUrl}/timeline/${recordId}/medication`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const medicationItems = await response.json();

    if (
      !medicationItems ||
      (medicationItems.statusCode &&
        (medicationItems.statusCode === 401 ||
          medicationItems.statusCode === 500))
    ) {
      navigation.pop();
      return;
    }
    if (medicationItems) {
      medicationItems.sort((a, b) => {
        return moment(b.date) - moment(a.date);
      });
    }
    return medicationItems;
  } catch (err) {
    console.error(err);
  }
};

export const GetExistingTimelineItem = async (timelineId, recordId, token) => {
  if (timelineId && timelineId !== -1) {
    try {
      const response = await fetch(
        `${baseApiUrl}/timeline/${recordId}/${timelineId}`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      const timelineItem = await response.json();
      return timelineItem;
    } catch (err) {
      console.error(err);
    }
  }
};

export const UpdateTimelineItem = async (timelineId, recordId, token, body) => {
  try {
    if (!body.date || !body.date === "") {
      body.date = new Date(moment());
    }
    const bodyDto = new FormData();
    let response;
    if (timelineId && timelineId !== -1) {
      if (
        body.properties.attachments &&
        body.properties.attachments.length !== 0
      ) {
        const attachments = body.properties.attachments.map((attachment) => {
          if (attachment.uri.startsWith("https://")) {
            return attachment;
          }
          const attachmentType =
            attachment.type === "image" ? "image/jpeg" : "application/pdf";
          const extensionType = attachment.type === "image" ? "jpg" : "pdf";
          const file = {
            uri: attachment.uri,
            type: attachmentType,
            name: `${recordId}_attachment_${new Date().getMilliseconds()}.${extensionType}`,
          };
          bodyDto.append("attachments", file);
          return file;
        });
      }
      bodyDto.append("item", JSON.stringify(body));
      response = await fetch(
        `${baseApiUrl}/timeline/${recordId}/${timelineId}`,
        {
          method: "PUT",
          headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
          },
          body: bodyDto,
        }
      );
    } else {
      if (
        body.properties.attachments &&
        body.properties.attachments.length !== 0
      ) {
        const attachments = body.properties.attachments.map((attachment) => {
          if (attachment.uri.startsWith("https://")) {
            return attachment;
          }
          const attachmentType =
            attachment.type === "image" ? "image/jpeg" : "application/pdf";
          const extensionType = attachment.type === "image" ? "jpg" : "pdf";
          const file = {
            uri: attachment.uri,
            type: attachmentType,
            name: `${recordId}_attachment_${new Date().getMilliseconds()}.${extensionType}`,
          };
          bodyDto.append("attachments", file);
          return file;
        });
      }
      bodyDto.append("item", JSON.stringify(body));
      response = await fetch(`${baseApiUrl}/timeline/${recordId}`, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
        },
        body: bodyDto,
      });
    }

    const timelineItem = await response.json();
    return timelineItem;
  } catch (err) {
    console.error(err);
  }
};

export const GetRecord = async (recordId, token) => {
  try {
    const response = await fetch(`${baseApiUrl}/records/${recordId}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const record = await response.json();
    return record;
  } catch (err) {
    console.error(err);
  }
};

export const UpdateRecord = async (recordId, token, body) => {
  try {
    const bodyDto = new FormData();
    if (body.avatar && !body.avatar.startsWith("https://")) {
      const image = {
        uri: body.avatar,
        type: "image/jpeg",
        name: `${recordId}_avatar.jpg`,
      };

      bodyDto.append("avatar", image);
      const { avatar, ...noAvatar } = body;
      body = noAvatar;
    }

    bodyDto.append("record", JSON.stringify(body));
    const response = await fetch(`${baseApiUrl}/records/${recordId}`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: "application/json",
        "Content-Type": "multipart/form-data",
      },
      body: bodyDto,
    });
    const record = await response.json();
    return record;
  } catch (err) {
    console.error(err);
  }
};

export const SendPasswordRecoveryEmail = async (email) => {
  const token = await fetch(`${baseApiUrl}/auth/recover/${email}`,{
    method: "GET",
  }).then((response)=> response.json()).catch((err)=>console.log(err))

  return token;
};

export const UpdatePassword = async (password, token) => {
  try {
    const response = await fetch(`${baseApiUrl}/auth/reset`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ password: password }),
    });
    const user = await response.json();
    return user;
  } catch (err) {
    console.error(err);
  }
};

export const ValidatePassword = async (password, token) => {
  try {
    const response = await fetch(`${baseApiUrl}/auth/validate/${password}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const validated = await response.json();
    if (validated.statusCode === 200) {
      return true;
    }
    return false;
  } catch (err) {
    console.error(err);
  }
};

export const GetUserByEmail = async (email, provider) => {
    try{    
        const response = await fetch(`${baseApiUrl}/${provider}/${email}`, {
          method: "GET",
        })
        const user = await response.json();
        console.log({ getuser: user });
        if (user.provider && user.provider === provider) {
          return user;
        }
        return null;
    } catch (TypeError) {
      console.log(`Caught TypeError in try and Block ${TypeError}`)
      return null;
    }
};

export const GetDoctorByUserId = async (userId, token) => {
  try {
    const response = await fetch(`${baseApiUrl}/doctors/user/${userId}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (
      (await response.clone().text()) !== "" &&
      response._bodyText !== "" &&
      response.status !== 404
    ) {
      const doctor = await response.json();
      return doctor;
    }
    return null;
  } catch (err) {
    console.error("Encountered error in function GetDoctorByUserId:", err);
  }
};

export const CreateUser = async (body) => {
  try {
    const response = await fetch(`${baseApiUrl}/auth/register`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        firstName: body.firstName,
        lastName: body.lastName || "",
        email: body.email.toUpperCase(),
        password: body.password,
      }),
    });
    const user = await response.json();
    return user;
  } catch (err) {
    console.error(err);
  }
};

export const ValidateToken = async (token) => {
  const response = await fetch(`${baseApiUrl}/auth/validate`, {
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  const result = await response.json();
  if (result.statusCode === 200) {
    return true;
  }
  return false;
};

export const GetUserToken = async () => {
  try {
    const userToken = JSON.parse(await AsyncStorage.getItem("UserToken"));
    return userToken;
  } catch (err) {
    console.error(err);
  }
};

export const SetUserToken = async (token, user, record) => {
  try {
    const doctor = await GetDoctorByUserId(user._id, token);
    await AsyncStorage.setItem(
      "UserToken",
      JSON.stringify({
        id: user._id,
        email: user.email,
        token,
        type: doctor ? "Doctor" : "Patient",
        record: record,
        provider: user.provider,
      })
    );
  } catch (err) {
    console.error(err);
  }
};

export const RevokeUserToken = async () => {
  try {
    await AsyncStorage.removeItem("UserToken");
  } catch (err) {
    console.error(err);
  }
};

export const CheckEmail = (email) => {
  if (!email.includes("@" && ".")) {
    return "Please enter a valid email address";
  }
  return "";
};

export const CheckPassword = (password) => {
  if (!password.length > 0) {
    return "Please enter a valid password";
  }
  return "";
};

export const CheckInputLength = (text, minLength, response) => {
  if (text.length < minLength) {
    return response;
  }
  return "";
};

export const CheckMatchingFields = (firstField, secondField, response) => {
  if (secondField !== firstField) {
    return response;
  }
  return "";
};

export const GetDoctors = async (token, recordId) => {
  try {
    const response = await fetch(
      `${baseApiUrl}/permissions/patient/${recordId}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        }
      }
    );
    const doctors = await response.json();
    if (doctors) {
      doctors.sort((a, b) => {
        let docA = `${a.firstName} ${a.lastName}`;
        let docB = `${b.firstName} ${b.lastName}`;
        if (docA === docB) {
          return 0;
        }
        return docA < docB ? -1 : 1;
      });
    }

    return doctors;
  } catch (err) {
    console.error(err);
  }
};

export const GrantDoctorPermissions = async (token, body) => {
  try {
    const response = await fetch(`${baseApiUrl}/permissions`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
    const permission = await response.json();
    return permission;
  } catch (err) {
    console.error(err);
  }
};

export const RevokeDoctorPermissions = async (token, body) => {
  const response = await fetch(`${baseApiUrl}/permissions`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  });
  const permission = await response.json();
  return permission;
};

export const CheckDoctorPermissions = async (body) => {
  try {
    const response = await fetch(`${baseApiUrl}/permissions/contact`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
    const permission = await response.text();
    return permission === "true" ? true : false;
  } catch (err) {
    console.error(err);
  }
};

export const GetPatients = async (token, doctorId) => {
  try {
    const response = await fetch(
      `${baseApiUrl}/permissions/doctor/${doctorId}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        }
      }
    );
    const patients = await response.json();
    if (Array.isArray(patients)) {
      patients.sort((a, b) => {
        let patA = `${a.firstName} ${a.lastName}`;
        let patB = `${b.firstName} ${b.lastName}`;
        if (patA === patB) {
          return 0;
        }
        return patA < patB ? -1 : 1;
      });
    }

    return patients;
  } catch (err) {
    console.error(err);
  }
};

export const GetQRCode = async (token) => {
  try {
    const response = await fetch(`${baseApiUrl}/doctors/current/qr`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const qrCode = await response.text();
    return qrCode;
  } catch (err) {
    console.error(err);
  }
};

export const SignInWithGoogle = async () => {
  try {
    const { type, accessToken } = await Google.logInAsync({
      iosClientId: iosClientId,
      androidClientId: androidClientId,
      iosStandaloneAppClientId: iosClientId,
      androidStandaloneAppClientId: androidClientId,
      scopes: ["profile", "email"],
    });
    if (type === "success") {
      const response = await fetch(googleApiUrl, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });
      const googleUser = await response.json();
      const name = googleUser.name.split(" ");
      console.log({ googleUser });

      const existingUser = await GetUserByEmail(googleUser.email, "google");
      console.log({ existingUser });

      let user = {};
      if (!existingUser) {
        user = await ValidateRegisterProvider("google", {
          email: googleUser.email.trim(),
          firstName: name[0],
          lastName: name[1] || "",
          providerId: googleUser.id,
        });
      } else {
        user = await ValidateLoginProvider({
          provider: "google",
          email: googleUser.email,
        });
      }
      console.log(user);
      return user;
    }
  } catch (error) {
    console.log(error.message);
    Alert.alert("Google Login Error", JSON.stringify([error, error.message]));
  }
};

export const SignUpWithGoogle = async () => {
  try {
    const { type, accessToken } = await Google.logInAsync({
      iosClientId: iosClientId,
      androidClientId: androidClientId,
      iosStandaloneAppClientId: iosClientId,
      androidStandaloneAppClientId: androidClientId,
      scopes: ["profile", "email"],
    });
    if (type === "success") {
      const response = await fetch(googleApiUrl, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });
      const googleUser = await response.json();
      const name = googleUser.name.split(" ");

      const user = await ValidateRegisterProvider("google", {
        email: googleUser.email.trim(),
        firstName: name[0],
        lastName: name[1] || "",
        providerId: googleUser.id,
      });
      return user;
    }
  } catch ({ message }) {
    alert(`Google Sign up Error: ${message}`);
  }
};

export const SignOutWithGoogle = async () => {
  try {
    //sign out with google
  } catch ({ message }) {
    alert(`Google Sign out Error: ${message}`);
  }
};

const SignInWithFacebookAccessToken = async (token) => {
  const response = await fetch(
    `${facebookApiUrl}?${facebookApiFields}&access_token=${token}`
  );
  const facebookUser = await response.json();
  const name = facebookUser.name.split(" ");

  const existingUser = await GetUserByEmail(facebookUser.email, "facebook");
  let user;
  if (!existingUser) {
    user = await ValidateRegisterProvider("facebook", {
      email: facebookUser.email.trim(),
      firstName: name[0],
      lastName: name[1] || "",
      providerId: facebookUser.id,
    });
  } else {
    user = await ValidateLoginProvider({
      provider: "facebook",
      email: facebookUser.email,
    });
  }
  return user;
};

export const SignInWithFacebook = async () => {
  const redirectUrl = AuthSession.getRedirectUrl();
  const result = await AuthSession.startAsync({
    authUrl:
      `https://www.facebook.com/v6.0/dialog/oauth?response_type=token` +
      `&client_id=${facebookAppId}` +
      "&state=fehwiugfvcdyu" +
      `&redirect_uri=${redirectUrl}` +
      `&scope=email`,
  }).then(()=>SignInWithFacebookAccessToken(result.params.access_token)).catch(()=>false);
  return result;
};

export const SignUpWithFacebook = async () => {
  try {
    const { type, token } = await Facebook.logInWithReadPermissionsAsync({
      permissions: ["public_profile", "email"],
    });
    if (type === "success") {
      const response = await fetch(
        `${facebookApiUrl}?${facebookApiFields}&access_token=${token}`
      );
      const facebookUser = await response.json();

      if (!facebookUser.email) alert("Email must be provided");

      const [firstName, lastName] = facebookUser.name.split(" ");

      const user = await ValidateRegisterProvider("facebook", {
        email: facebookUser.email.trim(),
        firstName,
        lastName: lastName || "",
        providerId: facebookUser.id,
      });
      return user;
    } else {
      //type === cancel
    }
  } catch ({ message }) {
    alert(`Facebook Sign up Error: ${message}`);
  }
};

export const SignOutWithFacebook = async () => {
  try {
  } catch ({ message }) {
    alert(`Facebook Sign out Error: ${message}`);
  }
};

export const SignInWithApple = async () => {
  try {
    const appleCredentials = await AppleAuthentication.signInAsync({
      requestedScopes: [
        AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
        AppleAuthentication.AppleAuthenticationScope.EMAIL,
      ],
    });
    let user = await ValidateLoginProvider({
      provider: "apple",
      providerId: appleCredentials.user,
    });
    if (user.statusCode === 401) {
      const response = await ValidateRegisterProvider("apple", {
        email: appleCredentials.email.trim(),
        firstName: appleCredentials.fullName.givenName,
        lastName: appleCredentials.fullName.familyName || "",
        providerId: appleCredentials.user,
      });
      console.log("user", user);
      if (response.statusCode === 401) {
        throw new Error(response.message);
      }
      user = response;
    }
    console.log("user", user);
    return user;
  } catch (e) {
    if (e.code === "ERR_CANCELED") {
      // handle that the user canceled the sign-in flow
      console.error(e);
    } else {
      // handle other errors
      console.error(e);
    }
  }
};

export const CreateMarketPermissions = async (
  user,
  token,
  skinData,
  eyeData,
  heartData,
  brainData
) => {
  try {
    const response = await fetch(`${baseApiUrl}/marketpermissions`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ user, skinData, eyeData, heartData, brainData }),
    });
    const result = await response.json();

    return result;
  } catch ({ message }) {
    alert(`Provider Validation Error: ${message}`);
  }
};

export const GetPermissions = async (user, token) => {
  try {
    const response = await fetch(
      `${baseApiUrl}/marketpermissions/getpermissions`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ user }),
      }
    );
    const result = await response.json();
    return result;
  } catch ({ message }) {
    alert(`Provider Validation Error: ${message}`);
  }
};

export const ValidateLoginProvider = async (userDetails) => {
  try {
    //maybe replace alert with modal later on for styling
    const response = await fetch(`${baseApiUrl}/auth/login`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        ...userDetails,
        ...(userDetails.email && { email: userDetails.email.trim() }),
      }),
    });
    const user = await response.json();

    if (user && user.statusCode && user.statusCode === 400) {
      return alert(user.message);
    }

    return user;
  } catch ({ message }) {
    alert(`Provider Validation Error: ${message}`);
  }
};

export const ValidateRegisterProvider = async (provider, body) => {
  try {
    //maybe replace alert with modal later on for styling
    const response = await fetch(`${baseApiUrl}/auth/register`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        provider,
        ...body,
      }),
    });
    const user = await response.json();
    if (user && user.statusCode && user.statusCode === 400) {
      return alert(user.message);
    }

    return user;
  } catch (err) {
    alert(`Provider Validation Error: ${message}`);
  }
};

export const UploadFile = async (recordId, token, body, filename) => {
  try {
    const bodyDto = new FormData();
    if (body) {
      const attachmentType =
        body.type === "image" ? "image/jpeg" : "application/pdf";
      const extensionType = body.type === "image" ? "jpg" : "pdf";
      const file = {
        uri: body.uri,
        type: attachmentType,
        name: `${recordId}_file_${new Date().getMilliseconds()}.${extensionType}`,
      };
      bodyDto.append("file", file);
      bodyDto.append("display", filename);
      const response = await fetch(`${baseApiUrl}/records/${recordId}/files`, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
        },
        body: bodyDto,
      });
      const uploadedFile = await response.json();
      return uploadedFile;
    }
  } catch (err) {
    console.error(err);
  }
};

export const GetFilePayload = async (recordId, token, navigation) => {
  try {
    const response = await fetch(`${baseApiUrl}/records/${recordId}/files`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const files = await response.json();

    if (
      !files ||
      (files.statusCode &&
        (files.statusCode === 401 || files.statusCode === 500))
    ) {
      navigation.pop();
      return;
    }

    if (files) {
      //sort files somehow?
      //perhaps by date using splice("_").pop()
    }
    return files;
  } catch (err) {
    console.error(err);
  }
};

export const DeleteFile = async (recordId, token, fileId) => {
  try {
    const response = await fetch(
      `${baseApiUrl}/records/${recordId}/files/${fileId}`,
      {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const file = await response.json();
    return file;
  } catch (err) {
    console.error(err);
  }
};

export const SendFormToDoctorEmail = async (
  token,
  email,
  dateOfInjury,
  formType,
  body
) => {
  try {
    dateOfInjury = dateOfInjury
      ? moment(dateOfInjury).format("LL")
      : moment().format("LL");
    await fetch(`${baseApiUrl}/doctors/email/${formType}`, {
      // const response = await fetch(`${baseApiUrl}/doctors/email/${formType}`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        ...body,
        email,
        dateOfInjury: dateOfInjury,
        dateOfAssessment: moment().format("LL"),
      }),
    });
    return;
    //Add this when we want to return for confirmation
    // const confirmation = await response.json();
    // return confirmation;
  } catch (err) {
    console.error(err);
  }
};

export const VmoRequest = async (vmo, comment) => {
  const bodyDto = new FormData();
  const date=new Date();

  let today= `${date.getDate()}-${date.getMonth()+1}-${date.getFullYear()}  :  ${date.getHours()}-${date.getMinutes()}-${date.getSeconds()}`;
  if (vmo && !vmo.startsWith("https://")) {
    const image = {
      uri: vmo,
      type: "image/jpeg",
      name: "$Test"+today+".jpg",
    }
    bodyDto.append("image", image);
    bodyDto.append("comment", comment);
    bodyDto.append("date", today);
  }
  
  const response = await fetch(`${baseApiUrl}/claims`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "multipart/form-data",
    },
    body: bodyDto,
  });
  const record = await response.json().catch(err=>console.log(err)).then(()=> record);
  
};

